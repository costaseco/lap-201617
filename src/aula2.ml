print_string "In�cio da Aula 2\n";;

(************************************************************
13 - Infira o tipo de cada uma das seguintes fun��es:
*)

let f1 x = x + 1 ;;
let f2 x = f1 x ;;
let f3 x = 1 + x 5 ;;
let f4 x y = x < y x ;; (* Veja o tipo de "<" aqui *)


(************************************************************
14 - Escreva em OCaml uma fun��o que produza a lista dos sucessores 
     duma lista de inteiros.

succAll : int list -> int list		
*)

let rec succAll l = assert false;;

(* Testes: *)

assert (succAll [] = []);;
assert (succAll [3; 6; 1; 0; -4] = [4; 7; 2; 1; -3]);;
assert (succAll [1] = [2]);;

(************************************************************)

(************************************************************
15 - Implemente o tipo de dados conjunto. Para representar os 
     conjuntos, use listas n�o ordenadas mas sem repeti��es. 
		
    As fun��es pretendidas s�o as seguintes:

    belongs: 'a -> 'a list -> bool       (* teste de perten�a *) 
*)

let rec belongs x l = assert false;; 

assert (belongs 4 [1;2;3;4;5;6] = true );;
assert (belongs 4 [1;2] = false );;
assert (belongs 4 [1] = false );;
assert (belongs 3 [] = false	);;
assert (belongs 4 [4] = true );;
assert (belongs [] [] = false);;
assert (belongs [] [[]] = true);;
assert (belongs [1] [[1];[1;2];[]] = true);;

(*    union: 'a list -> 'a list -> 'a list (* uni�o de conjuntos *)*)

let rec union l1 l2 = assert false;; 

assert (union [] [1;2;3] = [1;2;3]);;
assert (union [] [] = []);;
assert (union [7;3;9] [2;1;9] = [7;3;2;1;9]);;


(*    inter: 'a list -> 'a list -> 'a list (* intersec��o de conjuntos *)*)

let rec inter l1 l2 = assert false;;

assert (inter [7;3;9] [2;1;9] = [9]);;
assert (inter [] [2;1;9] = []);;
assert (inter [7;3;9] [] = []);;
assert (inter [] [] = []);;
assert (inter [7;3;9] [2;1] = []);;


(*    diff: 'a list -> 'a list -> 'a list  (* diferen�a de conjuntos *)*)

let rec diff l1 l2 = assert false;;

assert (diff [7;3;9] [2;1;9] = [7;3]);;

(*    power: 'a list -> 'a list list       (* conjunto pot�ncia: conjunto dos subconjuntos - dif�cil *)*)


let rec power l = assert false;;

assert (power [] = [[]]);;
assert (power [2;3] = [[]; [3]; [2]; [2;3]]);;
assert (power [1;2;3] = [[]; [3]; [2]; [2;3]; [1]; [1;3]; [1;2]; [1;2;3]]);; (* n�o interessa a ordem dos elementos *)

(*
Nota: Nas fun��es que recebem duas listas pode surgir a d�vida: 
faz-se a redu��o a problemas mais simples no primeiro ou no segundo 
argumentos da fun��o? Geralmente tenta fazer-se a redu��o usando o 
primeiro argumento e chega-se a uma solu��o satisfat�ria. Apenas 
em casos raros � necess�rio fazer a redu��o no segundo argumento, 
ou at� nos dois argumentos ao mesmo tempo.
*************************************************************)


(************************************************************
16 - Escreva em OCaml uma fun��o que, dado um inteiro n, gere 
     a lista dos n primeiros n�meros naturais, ordenada 
     decrescentemente.
    
    nat : int -> int list
*) 

let rec nat n = assert false;;

assert (nat 0 = []);;

assert (nat 1 = [0]);;

assert (nat 2 = [1;0]);;

assert (nat 5 = [4;3;2;1;0]);;

(*
Nota: Este exerc�cio tem a ver com listas, mas repare que o que 
determina a estrutura duma fun��o s�o os seus argumentos. Como o 
argumento desta fun��o � um inteiro, � prov�vel que esta fun��o 
tenha uma estrutura parecida com a da fun��o fatorial, do exerc�cio 6.
*)

(************************************************************
17 - O Departamento de Engenharia do Ambiente possui um dispositivo 
    que regista a temperatura ambiente segundo a segundo. No final de cada 
    ciclo de 24 horas, o dispositivo envia para um computador a lista dos 
    86400 valores reais que representam as amostras do dia. Mas a temperatura 
    ambiente varia lentamente. Assim � de esperar que uma lista de temperaturas 
    contenha muitos valores repetidos consecutivos. Analisemos a parte inicial 
    duma tal lista:

    [10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.1;
     10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.1;
     10.1; 10.1; 10.1; 10.1; 10.0; 10.0; 10.0; 10.0; 10.0; ...]

Esta lista indica que � meia-noite a temperatura era de 10.1 graus cent�grados. 
Depois, a temperatura n�o sofreu altera��o sens�vel durante mais 24 segundos, 
mas ao 26� segundo baixou para os 10.0 graus cent�grados [Excitante, n�o �?!].

O que nos interessa aqui � a quest�o do armazenamento destas listas de 
temperaturas no computador. A verdade � que n�o vale a pena guardar listas t�o 
longas. � prefer�vel guard�-las numa forma compactada para aproveitar melhor 
a mem�ria.

a) Escreva em OCaml uma fun��o para compactar listas, substituindo cada 
subsequ�ncia de valores repetidos por um par ordenado que indica qual o 
valor que se repete e qual o comprimento da subsequ�ncia.

    pack : 'a list -> ('a * int) list
*)

let rec pack l = assert false;;

assert (pack [] = []);;

assert (pack [10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.0; 10.0; 10.1; 10.0] =
                   [(10.1, 7); (10.0, 2); (10.1, 1); (10.0,1)]);;

(*
b) Escreva agora uma fun��o para descompactar listas.
   unpack : ('a * int) list -> 'a list
*)

let rec unpack l = assert false;;

assert (unpack [] = []);;

assert (unpack [(10.1, 7); (10.0, 2); (10.1, 1); (10.0,1)] =
    [10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.1; 10.0; 10.0; 10.1; 10.0]);;



(************************************************************
18 - a) Escreva em OCaml uma fun��o de ordem superior sobre listas, que permita aplicar sucessivamente uma opera��o bin�ria associativa � esquerda a todos os elementos duma lista. A fun��o tamb�m precisa de receber o elemento neutro da opera��o.
fold_left : ('a -> 'b -> 'a) -> 'a -> 'b list -> 'a
Esbo�o do funcionamento da fun��o:
fold_left f neutro [a1; a2; ...; an] = f (... (f (f neutro a1) a2) ...) an
b) Escreva uma fun��o de ordem superior sobre listas, semelhante � anterior mas aplic�vel a opera��es bin�rias associativas � direita.
fold_right : ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b
Esbo�o do funcionamento da fun��o:
fold_right f [a1; a2; ...; an] neutro = f a1 (f a2 (... (f an neutro) ...))
c) Programe a fun��o de ordem superior map recorrendo a uma destas fun��es.

*)

(************************************************************
19 - Procure no manual de refer�ncia da linguagem OCaml (dispon�vel na 
    p�gina da cadeira, na coluna da esquerda - veja "Basic Operations") 
    a lista completa das opera��es associadas aos tipos bool, int, float, string e char.
*)

(************************************************************
20 - Para cada um dos seguintes tipos, invente uma fun��o com esse tipo:

f1: int -> (int->int)
*)

let f1 = assert false;;

(*
f2: (int->int) -> (int->int)
*)

let f2 = assert false;;

(*
f3: int -> float -> string -> char
*)

let f3 = assert false;; 




