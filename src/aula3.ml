(************************************************************
21 - Considere o tipo �rvore bin�ria em OCaml:
*)
type 'a tree = Nil | Node of 'a * 'a tree * 'a tree;;

(*
Escreva as seguintes tr�s fun��es sobre �rvores:
    howMany : 'a -> 'a tree -> int    (* Conta n�mero de ocorr�ncias do valor na �rvore *)
*)

let howMany x t = assert false;;

let t1 = Nil;;

let t2 = Node(0,t1,t1);;

let t3 = Node(1,t1,t1);;

let t4 = Node(0, t3, t3);;

let t5 = Node(1, t3, t2);;

assert (howMany 1 t1 = 0);;
assert (howMany 1 t2 = 0);;
assert (howMany 1 t3 = 1);;
assert (howMany 1 t4 = 2);;
assert (howMany 1 t5 = 2);;

(*
   eqPairs : ('a * 'a) tree -> int   (* Conta n�mero de pares com as duas componentes iguais *)
*)

let eqPairs t = assert false;;

let p1 = (1,2);;
let p2 = (3,3);;

let pt2 = Node(p1,Nil,Nil);;
let pt3 = Node(p2,Nil,Nil);;

assert (eqPairs Nil = 0);;

assert (eqPairs pt2 = 0);;

assert (eqPairs pt3 = 1);;


(*
treeToList : 'a tree -> 'a list   (* Converte �rvore em lista, por uma ordem qualquer *)
*)

let treeToList t = assert false;;
	
let t6 = Node(3,t5,t5);;
let t7 = Node(9,t6,t5);;

assert (treeToList Nil = []);;
assert (treeToList t3 = [1]);;
assert (treeToList t5 = [1;1;0]);;
assert (treeToList t6 = [3;1;1;0;1;1;0]);;
assert (treeToList t7 = [9;3;1;1;0;1;1;0;1;1;0]);;

(************************************************************
22 - Programe uma fun��o que determine se uma �rvore bin�ria est� ou n�o equilibrada. 
Uma �rvore bin�ria diz-se equilibrada se, para cada n�, a diferen�a de profundidades 
das suas sub�rvores n�o superar a unidade. (Copie a fun��o a fun��o auxiliar height da te�rica 4.)

    balanced: 'a tree -> bool
*)

let balanced t = assert false;;
 
assert (balanced Nil = true);;
assert (balanced (Node(3,Node(5,Nil,Nil),Node(6,Nil,Nil))) = true);;
assert (balanced (Node(1,Nil,Node(2,Nil,Node(3,Nil,Nil)))) = false);;

(************************************************************
23 - Programe uma fun��o que produza a lista de todas as 
sub�rvores distintas que ocorrem na �rvore argumento.

	subtrees: 'a tree -> 'a tree list
*)

let subtrees t = assert false;;

assert (subtrees (Node(5,Nil,Node(6,Nil,Nil))) = [Node(5,Nil,Node(6,Nil,Nil)); Node(6,Nil,Nil); Nil]);;
assert (subtrees Nil = [Nil]);;


(************************************************************
24 - Primavera. Programe uma fun��o que fa�a crescer novas folhas 
	em todos os pontos duma �rvore onde esteja Nil. As novas 
	folhas s�o todas iguais entre si.
	
	spring: 'a -> 'a tree -> 'a tree
*)

let spring x t = assert false;;

assert (spring 3 Nil = Node(3,Nil,Nil));;
assert (spring 5 (Node(3,Nil,Nil)) = Node(3,Node(5,Nil,Nil),Node(5,Nil,Nil)));;
assert (spring 7 (Node(3,Node(5,Nil,Nil),Node(5,Nil,Nil))) 
						= Node(3,Node(5,Node(7,Nil,Nil),Node(7,Nil,Nil)),Node(5,Node(7,Nil,Nil),Node(7,Nil,Nil))));;

(************************************************************
25 - Outono. Programe uma fun��o que elimine todas as folhas 
existentes duma �rvore. Os n�s interiores que ficam a descoberto 
tornam-se folhas, claro, as quais j� n�o s�o para eliminar.

	fall: 'a tree -> 'a tree
*)

let fall t = assert false;;

assert (fall Nil = Nil);;
assert (fall (Node(3,Nil,Nil)) = Nil);;
assert (fall (Node(3,Node(5,Nil,Nil),Node(5,Nil,Nil))) = (Node(3,Nil,Nil)));;
assert (fall (Node(3,Node(5,Node(7,Nil,Nil),Node(7,Nil,Nil)),
                     Node(5,Node(7,Nil,Nil),Node(7,Nil,Nil)))) = Node(3,Node(5,Nil,Nil),Node(5,Nil,Nil)));; 


(************************************************************
26 - Repita os exerc�cios 23, 24 e 25, mas agora para �rvores n-�rias. 
O tipo �rvore n-�ria em OCaml define-se assim:

Nota: Uma folha duma �rvore bin�ria tem dois Nil por baixo, 
mas uma folha duma �rvore n-�ria costuma ter apenas uma lista 
vazia de filhos, portanto sem qualquer Nil.
*)

type 'a ntree = NNil | NNode of 'a * 'a ntree list

(*
ntreeToList : 'a ntree -> 'a list
*)

let ntreeToList t = assert false;;


(*
nsubtrees : 'a ntree -> 'a ntree list
*)

let nsubtrees t = assert false;;

(*
nspring: 'a -> 'a ntree -> 'a ntree
*)

let nspring x t = assert false;;

(*
nfall: 'a ntree -> 'a ntree
*)

let nfall t = assert false;;

(************************************************************
27 - Defina um tipo soma exp para representar express�es alg�bricas
 com uma vari�vel real "x". Nas express�es devem poder ocorrer os
 operadores bin�rios "+", "-", "*", "/", o operador un�rio "-", 
a vari�vel "x" e ainda literais reais. Exemplos de express�es:
    2*x2+5
    2*x7+5*(x-5)2-3
		
Depois de definido o tipo usando um tipo soma como este *)

type exp =
      Add of exp*exp
    | Sub of exp*exp
    | Mult of exp*exp
    | Div of exp*exp
    | Power of exp*int
    | Sim of exp
    | Const of float
    | Var
;;

(* escreva as seguintes fun��es sobre express�es alg�bricas:

a) eval: float -> exp -> float    (* Avalia a express�o no ponto dado *)
*)

let eval x e = assert false;; 

(*
b) deriv: exp -> exp              (* Determina a derivada em ordem a "x". N�o simplifique o resultado. *)
*)

let deriv e = assert false;; 

(*
Usando esta representa��o, a express�o 2*x2+5 escreve-se: .
*)

let e = Add(Mult(Const 2.0,Power(Var,2)),Const 5.0);;

assert (eval 2 e = 13);;
